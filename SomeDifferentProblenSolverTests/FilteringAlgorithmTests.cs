﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using GraphLogic;
using SomeDifferentProblenSolver;
using NUnit.Framework;

namespace SomeDifferentProblenSolverTests
{
    [TestFixture]
    public class FilteringAlgorithmTests
    {
        public static IEnumerable<TestCaseData> GetConnectedComponentsTestData
        {
            get
            {
                yield return new TestCaseData(
                    new int[5][]
                    {
                        new[] {1, 2},
                        new[] {0, 2},
                        new[] {0, 1},
                        new[] {4},
                        new[] {3},
                    }, new[] {3, 2}, 2);
                yield return new TestCaseData(
                    new int[5][]
                    {
                        new[] {1},
                        new[] {0},
                        new[] {3, 4},
                        new[] {2, 4},
                        new[] {2, 3},
                    }, new[] {3, 2}, 2);
                yield return new TestCaseData(
                    new int[5][]
                    {
                        new[] {1, 2},
                        new[] {0, 2},
                        new[] {0, 1},
                        new int[] {},
                        new int[] {},
                    }, new[] {3, 1}, 3);
            }
        }

        [TestCaseSource(nameof(GetConnectedComponentsTestData))]
        [Test]
        public void GetConnectedComponents_Graph_ComponentsExpected
            (int[][] rebra, int[] compSizes, int expectedCompCount)
        {
            //arrange
            Graph g = new Graph(rebra.GetLength(0));
            for (int i = 0; i < rebra.Length; i++)
            {
                Node node = new Node
                    (new int[0], rebra[i], i, i);
                g[i] = node;
            }
            HashSet<int> expectedSizes = new HashSet<int>(compSizes);
            //act
            IEnumerable<Graph> components = g.GetConnectedComponents();
            //assert
            Assert.AreEqual(expectedCompCount, components.Count());
            HashSet<int> actualSizes = new HashSet<int>();
            foreach (var comp in components)
                actualSizes.Add(comp.Count);
            CollectionAssert.AreEquivalent(expectedSizes, actualSizes);
        }

        public static IEnumerable<TestCaseData> TabuListTestData
        {
            get
            {
                yield return new TestCaseData(
                    new[,]
                    {
                        {1, 2},
                        {2, 3},
                        {1, 3},
                        {2, 4},
                        {3, 5},
                        {5, 9}
                    },
                    new[] {1, 5, 7, 4, 5, 3},
                    6,
                    new[,]
                    {
                        {1, 3}
                    },
                    new[,]
                    {
                        {1, 2},
                        {2, 3},
                        {2, 4},
                        {3, 5},
                        {5, 9}
                    });
                yield return new TestCaseData(
                    new[,]
                    {
                        {1, 2},
                        {2, 3},
                        {1, 3},
                        {2, 4},
                        {3, 5},
                        {5, 9}
                    },
                    new[] {1, 5, 7, 4, 5, 3},
                    1,
                    new[,]
                    {
                        {2, 3},
                        {1, 3},
                        {2, 4},
                        {3, 5},
                        {5, 9}
                    },
                    new[,]
                    {
                        {1, 2},
                    });
            }
        }

        [TestCaseSource(nameof(TabuListTestData))]
        [Test]
        public void RemoveFromAndIsInTabuList_TabuColoringsColoringsToDelete_ExpectedReaminingColorings
        (int[,] colorings, int[] removeTimes, int iterationsNumber, int[,] expectedColorings,
            int[,] notExpectedColorings)
        {
            //arrange
            var list = new LinkedList<Tuple<int, int, int>>();
            for (int i = 0; i < removeTimes.Length; i++)
            {
                list.AddLast(new Tuple<int, int, int>
                    (removeTimes[i], colorings[i, 0], colorings[i, 1]));
            }
            //act
            for (int i = 0; i <= iterationsNumber; i++)
                FilteringAlgorithm.RemoveFromTabuList(i, list);
            //assert
            for (int i = 0; i < expectedColorings.GetLength(0); i++)
                Assert.AreEqual
                (true, FilteringAlgorithm.IsInTabuList
                    (list, expectedColorings[i, 0], expectedColorings[i, 1]));
            for (int i = 0; i < notExpectedColorings.GetLength(0); i++)
                Assert.AreEqual
                (false, FilteringAlgorithm.IsInTabuList
                    (list, notExpectedColorings[i, 0], notExpectedColorings[i, 1]));
        }

        public static IEnumerable<TestCaseData> Func1TestData
        {
            get
            {
                Graph g;
                g = new Graph(4)
                {
                    [0] = new Node(new int[0], new[] {1, 2}, 0, 1),
                    [1] = new Node(new int[0], new[] {0, 2}, 1, 1),
                    [2] = new Node(new int[0], new[] {0, 1, 3}, 2, 2),
                    [3] = new Node(new int[0], new[] {2}, 3, 3)
                };
                yield return new TestCaseData(g, 2);
                g = new Graph(4)
                {
                    [0] = new Node(new int[0], new[] {1, 2}, 0, 1),
                    [1] = new Node(new int[0], new[] {0, 2}, 1, 1),
                    [2] = new Node(new int[0], new[] {0, 1, 3}, 2, 2),
                    [3] = new Node(new int[0], new[] {2}, 3, 2)
                };
                yield return new TestCaseData(g, 4);
                g = new Graph(4)
                {
                    [0] = new Node(new int[0], new[] {1, 2}, 0, 1),
                    [1] = new Node(new int[0], new[] {0, 2}, 1, 4),
                    [2] = new Node(new int[0], new[] {0, 1, 3}, 2, 2),
                    [3] = new Node(new int[0], new[] {2}, 3, 3)
                };
                yield return new TestCaseData(g, 0);
                g = new Graph(4)
                {
                    [0] = new Node(new int[0], new[] {1}, 0, 1),
                    [1] = new Node(new int[0], new[] {0}, 1, 2),
                    [2] = new Node(new int[0], new[] {3}, 2, 2),
                    [3] = new Node(new int[0], new[] {2}, 3, 3)
                };
                yield return new TestCaseData(g, 0);
                g = new Graph(4)
                {
                    [0] = new Node(new int[0], new[] {1, 2}, 0, 1),
                    [1] = new Node(new int[0], new[] {0, 2}, 1, 1),
                    [2] = new Node(new int[0], new[] {0, 1, 3}, 2, 2),
                    [3] = new Node(new int[0], new[] {2}, 3, 1)
                };
                yield return new TestCaseData(g, 2);
            }
        }

        [TestCaseSource(nameof(Func1TestData))]
        [Test]
        public static void Func1_Graph_F1valueExpected(Graph g, int expectedF1)
        {
            //act
            int f1 = FilteringAlgorithm.Func1(g);
            //
            Assert.AreEqual(expectedF1, f1);
        }

        public static IEnumerable<TestCaseData> RecountMinimizeFunctionData
        {
            get
            {
                Graph g;
                g = new Graph(4)
                {
                    [0] = new Node(new int[0], new[] {1, 2}, 0, 1),
                    [1] = new Node(new int[0], new[] {0, 2}, 1, 1),
                    [2] = new Node(new int[0], new[] {0, 1, 3}, 2, 1),
                    [3] = new Node(new int[0], new[] {2}, 3, 3)
                };
                yield return new TestCaseData(g, 2, 2, 2, 1, 3);
                g = new Graph(4)
                {
                    [0] = new Node(new int[0], new[] {1, 2}, 0, 1),
                    [1] = new Node(new int[0], new[] {0, 2}, 1, 1),
                    [2] = new Node(new int[0], new[] {0, 1, 3}, 2, 2),
                    [3] = new Node(new int[0], new[] {2}, 3, 2)
                };
                //graph nodenum oldCol curValue alpha newValue
                yield return new TestCaseData(g, 2, 1, 3, 1, 4);
                g = new Graph(4)
                {
                    [0] = new Node(new int[0], new[] {1, 2}, 0, 1),
                    [1] = new Node(new int[0], new[] {0, 2}, 1, 4),
                    [2] = new Node(new int[0], new[] {0, 1, 3}, 2, 2),
                    [3] = new Node(new int[0], new[] {2}, 3, 3)
                };
                yield return new TestCaseData(g, 1, 2, 2, 1, 0);
                g = new Graph(4)
                {
                    [0] = new Node(new int[0], new[] {1, 2}, 0, 1),
                    [1] = new Node(new int[0], new[] {0, 2}, 1, 1),
                    [2] = new Node(new int[0], new[] {0, 1, 3}, 2, 1),
                    [3] = new Node(new int[0], new[] {2}, 3, 3)
                };
                yield return new TestCaseData(g, 2, 2, 2, 0.5, 2.5);
                g = new Graph(4)
                {
                    [0] = new Node(new int[0], new[] {1, 2}, 0, 1),
                    [1] = new Node(new int[0], new[] {0, 2}, 1, 1),
                    [2] = new Node(new int[0], new[] {0, 1, 3}, 2, 2),
                    [3] = new Node(new int[0], new[] {2}, 3, 2)
                };
                //graph nodenum oldCol curValue alpha newValue
                yield return new TestCaseData(g, 2, 1, 3, 0.5, 3.5);
            }
        }

        [TestCaseSource(nameof(RecountMinimizeFunctionData))]
        [Test]
        public static void RecountFunction_GraphChangingColor_NewValueExpected
            (Graph g, int nodeNum, int oldColor, int curValue, double alpha, double newValue)
        {
            //act
            double actual = FilteringAlgorithm
                .RecountMinimizeFunction(g, g[nodeNum], curValue, alpha, oldColor);
            //assert
            Assert.LessOrEqual(Math.Abs(actual - newValue), 1e-6);
        }

        public static IEnumerable<TestCaseData> TabuSDTestData
        {
            get
            {
                //Граф из примера для редукции
                Graph graph = new Graph(12)
                {
                    [0] = new Node(new[] {3, 6}, new[] {5, 7}, 0),
                    [1] = new Node(new[] {1, 3, 4}, new[] {2, 5}, 1),
                    [2] = new Node(new[] {1, 3}, new[] {1, 6}, 2),
                    [3] = new Node(new[] {1, 2}, new[] {6, 10}, 3),
                    [4] = new Node(new[] {1, 6}, new[] {10}, 4),
                    [5] = new Node(new[] {1, 6}, new[] {0, 1, 7, 8}, 5),
                    [6] = new Node(new[] {1}, new[] {2, 3, 9, 10}, 6),
                    [7] = new Node(new[] {2, 3, 6}, new[] {0, 5}, 7),
                    [8] = new Node(new[] {1, 2, 3, 4}, new[] {1, 5, 9}, 8),
                    [9] = new Node(new[] {1, 3}, new[] {6, 8}, 9),
                    [10] = new Node(new[] {1, 3, 4, 5}, new[] {3, 4, 6, 11}, 10),
                    [11] = new Node(new[] {3, 4}, new[] {10}, 11)
                };
                Tuple<int, int>[] invalidPointColorings =
                {
                    new Tuple<int, int>(1, 3),
                    new Tuple<int, int>(2, 1),
                    new Tuple<int, int>(3, 1),
                    new Tuple<int, int>(10, 1),
                    new Tuple<int, int>(9, 1),
                    new Tuple<int, int>(8, 3),
                };
                yield return new TestCaseData(graph, invalidPointColorings);
            }
        }

        [TestCaseSource(nameof(TabuSDTestData))]
        [Test]
        public static void TabuSD_Graph_ValidPointColoringListExpected
            (Graph g, Tuple<int, int>[] invalidPointColorings)
        {
            //act
            IEnumerable<Tuple<int, int>> actual = g.TabuSD(false);
            //assert
            Assert.AreNotEqual(null, actual);
            foreach (var pointColoring in actual)
            {
                Assert.AreEqual(false, invalidPointColorings.Contains(pointColoring));
            }
            Assert.AreEqual
                (invalidPointColorings.Distinct().Count(), invalidPointColorings.Length);
        }

        public static IEnumerable<TestCaseData> FilterAlgorithmFilterGraphTestData
        {
            get
            {
                //Граф из примера для редукции
                Graph graph = new Graph(7)
                {
                    [0] = new Node(new[] {1, 2, 3}, new[] {2, 3, 4, 5, 6}, 0),
                    [1] = new Node(new[] {1, 4, 5}, new[] {2, 3, 4, 5, 6}, 1),
                    [2] = new Node(new[] {1, 2, 3}, new[] {0, 1, 4, 5, 6}, 2),
                    [3] = new Node(new[] {1, 2, 4}, new[] {0, 1, 4, 5, 6}, 3),
                    [4] = new Node(new[] {1, 2, 3}, new[] {0, 1, 2, 3, 5, 6}, 4),
                    [5] = new Node(new[] {1, 2, 4}, new[] {0, 1, 2, 3, 4, 6}, 5),
                    [6] = new Node(new[] {1, 2, 5}, new[] {0, 1, 2, 3, 4, 5}, 6),
                };
                Dictionary<int, int> expectedPointColorings = new Dictionary<int, int>
                {
                    [0] = 1,
                    [1] = 1,
                    [2] = 2,
                    [3] = 2,
                    [4] = 3,
                    [5] = 4,
                    [6] = 5,
                };
                yield return new TestCaseData(graph, expectedPointColorings);
                graph = new Graph(4)
                {
                    [0] = new Node(new[] {1, 2}, new[] {2, 3}, 0),
                    [1] = new Node(new[] {1, 3}, new[] {2, 3}, 1),
                    [2] = new Node(new[] {1, 2}, new[] {0, 1, 3}, 2),
                    [3] = new Node(new[] {1, 3}, new[] {0, 1, 2}, 3),
                };
                expectedPointColorings = new Dictionary<int, int>
                {
                    [0] = 1,
                    [1] = 1,
                    [2] = 2,
                    [3] = 3,
                };
                yield return new TestCaseData(graph, expectedPointColorings);
            }
        }

        [TestCaseSource(nameof(FilterAlgorithmFilterGraphTestData))]
        [Test]
        public static void FilterGraph_UniqueColoredGraph_FilteredExpected
            (Graph g, Dictionary<int, int> expextedPointColorings)
        {
            //act
            Graph actual = g.FilterGraph();
            //assert
            Assert.AreEqual(g.Count, actual.Count);
            for (int i = 0; i < actual.Count; i++)
            {
                Assert.AreEqual(1, actual[i].Colors.Count);
                Assert.AreEqual(expextedPointColorings[actual[i].RealNodeNumber],
                    actual[i].Colors.First());
            }
        }

        public static IEnumerable<TestCaseData> UniqueNNodesFilteringData
        {
            get
            {
                int[] kParameters = {2, 3, 4, 5, 6, 7, 8, 9};
                foreach (var kParameter in kParameters)
                {
                    yield return new TestCaseData(kParameter);
                }
            }
        }

        [TestCaseSource(nameof(UniqueNNodesFilteringData))]
        [Test]
        public static void FilterGraph_KParameterGraphWithUniquePointColoring
            (int kParameter)
        {
            int nodeCount = 3*kParameter - 2;
            //Arrange
            Graph g = new Graph(nodeCount);
            Dictionary<int, int> expectedPointColorings = new Dictionary<int, int>();
            for (int i = 0; i < (kParameter - 1) * 2; i++)
            {
                int j = i;
                if ((i & 1) == 0)
                {
                    g[i] = new Node(Enumerable.Range(1, kParameter),
                        Enumerable.Range(0, nodeCount)
                            .Where(num => num != j && num != j + 1).ToArray()
                        , i);
                }
                else
                {
                    g[i] = new Node(Enumerable.Range(1, j/2 + 1)
                            .Union(Enumerable.Range(kParameter + 1,
                                kParameter - j/2 - 1)),
                        Enumerable.Range(0, nodeCount)
                            .Where(num => num != j && num != j - 1).ToArray()
                        , i);
                }
                expectedPointColorings[i] = i / 2 + 1;
            }
            for (int i = (kParameter - 1)*2, j = 0; i < nodeCount; i++, j++)
            {
                int number = i;
                g[i] = new Node(Enumerable.Range(1, kParameter - 1)
                    .Union(new []{kParameter + j}),
                    Enumerable.Range(0, nodeCount).Where(num => num != number),
                    i);
                expectedPointColorings[i] = kParameter + j;
            }

            Stopwatch sp = Stopwatch.StartNew();
            FilterGraph_UniqueColoredGraph_FilteredExpected(g, expectedPointColorings);
            sp.Stop();
            using (StreamWriter sw = new StreamWriter("uniqueStatistics", true))
            {
                sw.WriteLine($"{nodeCount} - {sp.Elapsed.TotalSeconds} seconds");
                sw.Flush();
            }
        }
    }
}
