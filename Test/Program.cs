﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphLogic;
using SomeDifferentProblenSolver;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
           /* Graph testGraph = new Graph(12);

            testGraph[0] = new Node(new List<int>(), new List<int>(), 0);
            testGraph[0].Adjacents.AddLast(1);
            testGraph[0].Adjacents.AddLast(2);
            testGraph[0].Colors.Add(3);
            testGraph[0].Colors.Add(6);

            testGraph[1] = new Node(new List<int>(), new List<int>(), 1);
            testGraph[1].Adjacents.AddLast(0);
            testGraph[1].Adjacents.AddLast(2);
            testGraph[1].Colors.Add(2);
            testGraph[1].Colors.Add(3);
            testGraph[1].Colors.Add(6);

            testGraph[2] = new Node(new List<int>(), new List<int>(), 2);
            testGraph[2].Adjacents.AddLast(0);
            testGraph[2].Adjacents.AddLast(1);
            testGraph[2].Adjacents.AddLast(3);
            testGraph[2].Adjacents.AddLast(4);
            testGraph[2].Colors.Add(1);
            testGraph[2].Colors.Add(6);

            testGraph[3] = new Node(new List<int>(), new List<int>(), 3);
            testGraph[3].Adjacents.AddLast(2);
            testGraph[3].Adjacents.AddLast(4);
            testGraph[3].Adjacents.AddLast(5);
            testGraph[3].Colors.Add(1);
            testGraph[3].Colors.Add(3);
            testGraph[3].Colors.Add(4);

            testGraph[4] = new Node(new List<int>(), new List<int>(), 4);
            testGraph[4].Adjacents.AddLast(2);
            testGraph[4].Adjacents.AddLast(3);
            testGraph[4].Adjacents.AddLast(6);
            testGraph[4].Colors.Add(1);
            testGraph[4].Colors.Add(2);
            testGraph[4].Colors.Add(3);
            testGraph[4].Colors.Add(4);

            testGraph[5] = new Node(new List<int>(), new List<int>(), 5);
            testGraph[5].Adjacents.AddLast(3);
            testGraph[5].Adjacents.AddLast(7);
            testGraph[5].Colors.Add(1);
            testGraph[5].Colors.Add(3);

            testGraph[6] = new Node(new List<int>(), new List<int>(), 6);
            testGraph[6].Adjacents.AddLast(4);
            testGraph[6].Adjacents.AddLast(7);
            testGraph[6].Colors.Add(1);
            testGraph[6].Colors.Add(3);

            testGraph[7] = new Node(new List<int>(), new List<int>(), 7);
            testGraph[7].Adjacents.AddLast(5);
            testGraph[7].Adjacents.AddLast(6);
            testGraph[7].Adjacents.AddLast(8);
            testGraph[7].Adjacents.AddLast(9);
            testGraph[7].Colors.Add(1);

            testGraph[8] = new Node(new List<int>(), new List<int>(), 8);
            testGraph[8].Adjacents.AddLast(9);
            testGraph[8].Adjacents.AddLast(7);
            testGraph[8].Colors.Add(1);
            testGraph[8].Colors.Add(2);

            testGraph[9] = new Node(new List<int>(), new List<int>(), 9);
            testGraph[9].Adjacents.AddLast(7);
            testGraph[9].Adjacents.AddLast(8);
            testGraph[9].Adjacents.AddLast(10);
            testGraph[9].Adjacents.AddLast(11);
            testGraph[9].Colors.Add(1);
            testGraph[9].Colors.Add(3);
            testGraph[9].Colors.Add(4);
            testGraph[9].Colors.Add(5);

            testGraph[10] = new Node(new List<int>(), new List<int>(), 10);
            testGraph[10].Adjacents.AddLast(9);
            testGraph[10].Colors.Add(1);
            testGraph[10].Colors.Add(6);

            testGraph[11] = new Node(new List<int>(), new List<int>(), 11);
            testGraph[11].Adjacents.AddLast(9);
            testGraph[11].Colors.Add(3);
            testGraph[11].Colors.Add(4);

            FilteringAlgorithm.DSatur(testGraph, 0);//ноль - фиктивный параметр
            FilteringAlgorithm.Reduction(testGraph);
            Console.WriteLine(testGraph);

            Graph testGraph2 = new Graph(4);

            testGraph2[0] = new Node(new List<int>(), new List<int>(), 0);
            testGraph2[0].Adjacents.AddLast(3);
            testGraph2[0].Adjacents.AddLast(2);
            testGraph2[0].Colors.Add(1);
            testGraph2[0].Colors.Add(2);

            testGraph2[1] = new Node(new List<int>(), new List<int>(), 1);
            testGraph2[1].Adjacents.AddLast(3);
            testGraph2[1].Adjacents.AddLast(2);
            testGraph2[1].Colors.Add(1);
            testGraph2[1].Colors.Add(3);

            testGraph2[2] = new Node(new List<int>(), new List<int>(), 2);
            testGraph2[2].Adjacents.AddLast(0);
            testGraph2[2].Adjacents.AddLast(1);
            testGraph2[2].Adjacents.AddLast(3);
            testGraph2[2].Colors.Add(1);
            testGraph2[2].Colors.Add(2);

            testGraph2[3] = new Node(new List<int>(), new List<int>(), 3);
            testGraph2[3].Adjacents.AddLast(2);
            testGraph2[3].Adjacents.AddLast(1);
            testGraph2[3].Adjacents.AddLast(0);
            testGraph2[3].Colors.Add(1);
            testGraph2[3].Colors.Add(3);

            Graph g = testGraph2.FilterGraph();*/
            LinkedList<int> nodesCounts = new LinkedList<int>();
            for (int i = 2; i <= 10; i++)
                nodesCounts.AddLast(i*10);
            double[] havingEdgeProbabilies = {0.1, 0.3, 0.6};
            int d = 300;
            int[] maxColorsSetSizes = {10, 20};
            LinkedList<Tuple<int, double, int, int, TimeSpan>> results = new LinkedList<Tuple<int, double, int, int, TimeSpan>>();
            foreach (var nodesCount in nodesCounts)
            {
                foreach (var havingEdgeProbability in havingEdgeProbabilies)
                {
                    foreach (var maxColorsSetSize in maxColorsSetSizes)
                    {
                        Graph graph = GenerateRandomGraph(nodesCount, havingEdgeProbability, d, maxColorsSetSize);
                        DateTime start = DateTime.Now;
                        graph.FilterGraph();
                        DateTime finish = DateTime.Now;
                        results.AddLast(new Tuple<int, double, int, int, TimeSpan>(nodesCount, havingEdgeProbability, d,
                            maxColorsSetSize, finish.Subtract(start)));
                    }
                }
            }
            using (StreamWriter streamWriter = new StreamWriter("RandomGraphResults4.txt"))
            {
                streamWriter.WriteLine("{0,-15} {1,-10} {2,-15} {3,-22} {4,-20}",
                    "NodesCount", "EdgeP", "ColorsCount", "MaxEdgeColorsCount", "Time");
                foreach (var res in results)
                {
                    streamWriter.WriteLine("{0,-15} {1,-10} {2,-15} {3,-22} {4,-20}",
                        res.Item1, res.Item2, res.Item3, res.Item4, res.Item5.TotalSeconds);
                }
                foreach (var havingEdgeProbability in havingEdgeProbabilies)
                {
                    foreach (var maxColorsSetSize in maxColorsSetSizes)
                    {
                        var tempRes =
                            results.Select(x => x).Where(x => x.Item2 == havingEdgeProbability && x.Item4 == maxColorsSetSize);
                        streamWriter.WriteLine("EdgeP = {0}, MaxEdgeColorsCount = {1}", havingEdgeProbability, maxColorsSetSize);
                        foreach (var t in tempRes)
                        {
                            streamWriter.Write("( {0}; {1} ) ", t.Item1, t.Item5.TotalSeconds);
                        }
                        streamWriter.WriteLine();
                    }
                }
            }
            Console.WriteLine();
        }

        private static Graph GenerateRandomGraph(int nodesCount, double havingEdgeProbability,
            int totalColorsCount, int maxColorsSetSize)
        {
            Random random = new Random();
            Graph graph = new Graph(nodesCount);
            for (int i = 0; i < graph.Count; i++)
            {
                graph[i] = new Node(new List<int>(), new List<int>(),  i);
            }
            foreach (var node in graph.nodes)
            {
                int colorsSetSize = random.Next(maxColorsSetSize) + 1;
                while (node.Colors.Count < colorsSetSize)
                {
                    node.Colors.Add(random.Next(totalColorsCount) + 1);
                }
                for (int i = node.RealNodeNumber + 1; i < graph.Count; i++)
                {
                    if (random.NextDouble() <= havingEdgeProbability)
                    {
                        node.Adjacents.AddLast(i);
                        graph[i].Adjacents.AddLast(node.RealNodeNumber);
                    }
                }
            }
            return graph;
        }
    }
}
