﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphLogic
{
    /// <summary>
    /// Вершина графа
    /// </summary>
    public class Node : ICloneable
    {
        /// <summary>
        /// Допустимые цвета
        /// </summary>
        public HashSet<int> Colors;
        /// <summary>
        /// Текущий цвет (может быть не задан = 0)
        /// </summary>
        public int Color { get; set; }
        /// <summary>
        /// Список номеров смежных вершин
        /// </summary>
        public LinkedList<int> Adjacents;

        public int RealNodeNumber { get; private set; }

        public Node(IEnumerable<int> colors, IEnumerable<int> adjacentes, int realNodeNumber, 
            int color = 0)
        {
            Colors = new HashSet<int>(colors);
            Adjacents = new LinkedList<int>(adjacentes);
            RealNodeNumber = realNodeNumber;
            Color = color;
        }

        public Node Clone()
        {
            return new Node(Colors, Adjacents, RealNodeNumber, Color);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
