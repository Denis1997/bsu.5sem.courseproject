﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GraphLogic
{
    public class Graph : ICloneable
    {
        /// <summary>
        /// Вершины графа, нумеруются с нуля, номер вершины в графе на единицу 
        /// больше её номера в массиве
        /// </summary>
        public Node[] nodes;

        /// <summary>
        /// Число вершин в графе
        /// </summary>
        public int Count => nodes?.Length ?? 0;

        public Graph(int count)
        {
            nodes = new Node[count];
        }

        public Graph(Graph graph)
        {
            nodes = new Node[graph.Count];
            for (int i = 0; i < Count; i++)
                this[i] = graph[i].Clone();
        }

        /// <summary>
        /// Индексатор для доступа к вершинам
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public Node this[int pos]
        {
            get { return nodes[pos]; }
            set { nodes[pos] = value; }
        }

        public Graph Clone()
        {
            Graph ret = new Graph(Count);
            for (int i = 0; i < Count; i++)
                ret[i] = this[i].Clone();
            return ret;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
