﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphLogic;

namespace SomeDifferentProblenSolver
{
    public static class FilteringAlgorithm
    {
        public static Random randGen = new Random();

        public static Graph FilterGraph(this Graph graph)
        {
            IEnumerable<Graph> gComponents = graph.Clone().TestColorability();
            if (ReferenceEquals(gComponents, null))
                return null;
            //добавим найденную раскраску в список допустимых (раскраски мы нашли
            //для компонент размера больше 2-х, с двумя мы и так справимся отлично
            HashSet<Tuple<int, int>> allowableRealPointColorings = new HashSet<Tuple<int, int>>();
            foreach (var component in gComponents)
            {
                if (component.Count <= 2)
                    continue;
                foreach (var node in component.nodes)
                {
                    allowableRealPointColorings.Add(
                        new Tuple<int, int>(node.RealNodeNumber, node.Color));
                }
            }
            HashSet<int> oneElementSet = new HashSet<int>();//нужен для фиксации цвета вершины
            foreach (Graph Gj in gComponents) //пробегаем все связные компоненты графа
            {
                //пополняем текущее множество раскрасок
                //как можно большим списком допустимых точечных раскрасок
                allowableRealPointColorings.UnionWith(Gj.TabuSD(false));
                for (int v = 0; v < Gj.Count; v++)
                {
                    HashSet<int> DvResult = Gj[v].Colors;
                    int[] vColors = DvResult.ToArray();
                    foreach (int i in vColors)
                    {
                        if (!DvResult.Contains(i))//возможно мы уже 
                            continue;   //отфильтровали этот цвет при редукции
                        if (allowableRealPointColorings.Contains
                            (new Tuple<int, int>(Gj[v].RealNodeNumber, i)))
                            continue; //уже выяснено, что цвет допустим
                        oneElementSet.Clear();
                        oneElementSet.Add(i);
                        Gj[v].Colors = oneElementSet;//фиксируем цвет вершины
                        if (Gj.Clone().TestColorability() == null)//если компонента стала не D-раскрашиваемой
                        {
                            DvResult.Remove(i);     //отфильтровываем цвет
                            Gj[v].Colors = DvResult; //возвращаем вершине её отфильтрованное множество цветов
                            Gj.Reduction();// редуцируем компоненту на предмет ещё большего её упрощения
                        }
                    }
                    Gj[v].Colors = DvResult;
                }
            }
            //восстанавливаем исходную структуру графа с редуцированным множеством цветов
            Graph ret = graph.Clone();
            foreach (var g in gComponents)
            {
                foreach (var node in g.nodes)
                {
                    ret[node.RealNodeNumber].Colors = node.Colors;
                }
            }
            return ret;
        }

        /// <summary>
        /// checks if <paramref name="graph"/> is D-colorable
        /// </summary>
        /// <returns>null if graph is not D-colorable, otherwise
        /// returns connectred components of G' with D' color set</returns>
        public static IEnumerable<Graph> TestColorability(this Graph graph)
        {
            if (!graph.Reduction())
                return null;
            IEnumerable<Graph> components = graph.GetConnectedComponents();
            foreach (var component in components)
            {
                if (component.Count <= 2)
                    continue;
                if (component.TabuSD(true).Any())
                    continue;

                //множество цветов допустимых для каких-либо вершин компоненты
                HashSet<int> compColorSet = new HashSet<int>();
                foreach (var node in component.nodes)
                {
                    compColorSet.UnionWith(node.Colors);
                }
                //конструируем граф Gj (+) D
                Graph Gplus = new Graph(component.Count + compColorSet.Count);
                int pos = component.Count;
                for (int i = 0; i < component.Count; i++)//переносим, то что уже есть
                    Gplus[i] = component[i].Clone();
                foreach (var color in compColorSet)//добавляем вершины клики
                {
                    Gplus[pos] = new Node(new int[0], new int[0], -1, color);
                    for (int i = pos - 1; i >= component.Count; i--)//добавляем доп рёбра в вершины, 
                                    //в множествах цветов который отсутствует цвет вершины из клики
                    {
                        Gplus[i].Adjacents.AddLast(pos);
                        Gplus[pos].Adjacents.AddLast(i);
                    }
                    for (int i = component.Count - 1; i >= 0; i--)
                    {
                        if (!Gplus[i].Colors.Contains(color))
                        {
                            Gplus[i].Adjacents.AddLast(pos);
                            Gplus[pos].Adjacents.AddLast(i);
                        }
                    }
                    pos++;
                }
                //Применяем DSatur
                if (Gplus.DSaturByDenis(component.Count) > compColorSet.Count)
                    return null;//если дсатур не смог норм раскрасить, 
                                //то никто не сможет - компонента не раскрашиваема
                for (int i = 0; i < component.Count; i++)//красим исходную компоненту в цвета дсатура
                    component[i].Color = Gplus[i].Color;
            }
            return components;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="checkDColorable"></param>
        /// <returns>Returns null if <paramref name="checkDColorable"/> is true if
        /// graph is D-colorable, otherwise returns most possible list of 
        /// point colorings</returns>
        public static IEnumerable<Tuple<int, int>> TabuSD
            (this Graph graph, bool checkDColorable)
        {
            Graph G = graph.Clone();
            long allPointColorings = G.nodes.Sum(n => n.Colors.Count);
            foreach (var node in G.nodes)       //начальное решение, выбрали какие-то цвета вершин
                node.Color = node.Colors.First();
            double alpha = 1;
            var allowablePointColorings = new HashSet<Tuple<int, int>>();//допустимые раскраски
            var tabuList = new LinkedList<Tuple<int, int, int>>();
            int DColorableCount = 0;            // статистика подряд идущих D-раскрашиваемых решений
            int NotDСolorableCount = 0;         // статистика подряд идущих решений с конфликтующими рёбрами
            //нужны для пересчёта коэффициента alpha
            //while no stopping criterion
            for (int itNumber = 0; itNumber < 2000 
                && allowablePointColorings.Count < allPointColorings; itNumber++)
            {
                RemoveFromTabuList(itNumber, tabuList);
                bool firstCond = false;         //для вычисления времени жизни в табу листе
                double fBest = double.MaxValue;
                double fOfC = MinimizeFunction(G, alpha, allowablePointColorings);
                int vBest = 0;
                int iBest = G[0].Color;
                for (int v = 0; v < G.Count; v++)//для каждой вершины, такой, что она граничит с конфликтующей или уже в L списке
                {
                    bool isOk = IsAlreadyAllowablePointColoring
                        (allowablePointColorings, G[v].RealNodeNumber, G[v].Color); //проверка, что в L-списке
                    bool tempFirstCond = isOk;
                    //если не содержится в списке
                    isOk = isOk || IsConficting(G, G[v]);
                    if (!isOk)//если неверно ни то, ни другое, то переходим к следующей вершине
                        continue;
                    foreach (int color in G[v].Colors)              //foreach color in D_v \ {c(v)}
                        if (color != G[v].Color)                    // i in Dv \ {c(v)}
                        {
                            if (!IsInTabuList(tabuList, G[v].RealNodeNumber, color))
                                //раскраска не должна быть заблокирована в табу-лист
                            {
                                int memColor = G[v].Color;
                                G[v].Color = color;
                                double fCur = RecountMinimizeFunction(G, G[v], fOfC, alpha, memColor);                              
                                G[v].Color = memColor;
                                if (fCur < fBest)
                                {
                                    vBest = v;
                                    iBest = color;
                                    fBest = fCur;
                                    firstCond = tempFirstCond;
                                }
                                if (fCur < fOfC)
                                    goto introduce;
                            }
                        }
                }
                introduce:
                //Introduce (vbest, ibest) in the tabu list
                bool isGoodPoint = false;
                //проверяем, что предыдущая раскраска точки была в списке допустимых
                //а новая ракраска еще не в списке
                if (firstCond && 
                    !allowablePointColorings.Contains
                        (new Tuple<int, int>(G[vBest].RealNodeNumber, iBest)))
                {
                    //проверяем, смежна ли точка с другими точками того же цвета
                    foreach (var adjacent in G[vBest].Adjacents)
                    {
                        if (G[adjacent].Color == iBest)
                            goto badPoint;
                    }
                    isGoodPoint = true;
                }
                badPoint:
                //добавляем точечную раскраску в табулист
                if (!IsInTabuList(tabuList, G[vBest].RealNodeNumber, iBest))
                    tabuList.AddLast(new Tuple<int, int, int>
                    (CountLifeTime(isGoodPoint, 
                        CountNumberOfNeighborsSolutions(G, allowablePointColorings, tabuList)),
                        G[vBest].RealNodeNumber, iBest));
                G[vBest].Color = iBest;
                if (Func1(G) == 0)
                {
                    for (int i = 0; i < G.Count; i++)
                    {
                        allowablePointColorings.Add
                            (new Tuple<int, int>(G[i].RealNodeNumber, G[i].Color));
                    }
                    if (checkDColorable)
                        return allowablePointColorings;//указывает, что граф d-раскрашиваемый
                    DColorableCount++;
                    NotDСolorableCount = 0;
                }
                else
                {
                    DColorableCount = 0;
                    NotDСolorableCount++;
                }
                //пересчитываем коэффициент
                if (DColorableCount == 10)
                {
                    DColorableCount = 0;
                    alpha /= 2;
                }
                if (NotDСolorableCount == 10)
                {
                    NotDСolorableCount = 0;
                    alpha *= 2;
                }
            }
            return allowablePointColorings;
        }

        public static bool IsAlreadyAllowablePointColoring
            (IEnumerable<Tuple<int, int>> allowableColorings, int nodeNumber, int color)
        {
            return allowableColorings.Contains(new Tuple<int, int>(nodeNumber, color));
        }

        public const int NotVisited = -1;
        public const int NotColored = -1;

        public static IEnumerable<Graph> GetConnectedComponents(this Graph g)
        {
            LinkedList<Graph> components = new LinkedList<Graph>();
            Node[] nodesOfComponent = new Node[g.Count];
            int componentSize = 0;
            foreach (var node in g.nodes)
            {
                node.Color = NotVisited;
            }
            for (int i = 0; i < g.nodes.Length; i++)
            {
                if (g[i].Color == NotVisited)
                {
                    componentSize = 0;
                    Dfs(g, g[i], nodesOfComponent, ref componentSize);
                    Graph component = new Graph(componentSize);
                    for (int j = 0; j < componentSize; j++)
                        component[j] = nodesOfComponent[j].Clone();
                    components.AddLast(component);
                }
            }
            return components;
        }

        public static void Dfs(Graph g, Node cur, Node[] nodes, ref int curNewNumber)
        {
            cur.Color = curNewNumber;
            nodes[curNewNumber++] = cur.Clone();
            nodes[cur.Color].Adjacents.Clear();
            foreach (var neighbor in cur.Adjacents)
            {
                if (g[neighbor].Color == NotVisited)
                    Dfs(g, g[neighbor], nodes, ref curNewNumber);
                nodes[cur.Color].Adjacents.AddLast(g[neighbor].Color);
            }
        }

        public static void RemoveFromTabuList
            (int itNumber, ICollection<Tuple<int, int, int>> tabuList)
        {
            var recsToRemove = tabuList.Where(rec => rec.Item1 == itNumber).ToArray();
            foreach (var item in recsToRemove)
            {
                tabuList.Remove(item);
            }
        }

        public static bool IsInTabuList
            (ICollection<Tuple<int, int, int>> tabuList, int pointNumber, int pointColor)
        {
            Tuple<int, int> pointColoring = new Tuple<int, int>(pointNumber, pointColor);
            foreach (var tabu in tabuList)
            {
                if (pointColoring.Item1 == tabu.Item2 && pointColoring.Item2 == tabu.Item3)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isGoodPoint">Значит не смежна с вершинами с таким же цветом
        /// и до этого не была в списке точечных раскрасок</param>
        /// <param name="Nc">Окружение решения</param>
        /// <returns></returns>
        public static int CountLifeTime(bool isGoodPoint, int Nc)
        {
            if (isGoodPoint)
                return randGen.Next(11) + 30 + 50*(int) Math.Sqrt(Nc);
            return randGen.Next(11) + 20 + (int) Math.Sqrt(Nc);
        }

        /// <summary>
        /// Counts Nc
        /// </summary>
        public static int CountNumberOfNeighborsSolutions(Graph g, 
            ICollection<Tuple<int, int>> allowablePointColorings,
            ICollection<Tuple<int, int, int>> tabuList)
        {
            int Nc = 0;
            for (int i = 0; i < g.Count; i++)
            {
                if (IsConficting(g, g[i]) ||
                        IsAlreadyAllowablePointColoring
                            (allowablePointColorings, g[i].RealNodeNumber, g[i].Color))
                    foreach (var color in g[i].Colors)
                    {
                        if (color == g[i].Color)
                            continue;
                        if (IsInTabuList(tabuList, g[i].RealNodeNumber, color))
                            continue;
                        Nc++;
                    }
            }
            return Nc;
        }

        public static int CountLocalNc(Graph g,
            ICollection<Tuple<int, int, int>> tabuList,
            ICollection<Tuple<int, int>> allowablePointColorings, int vBest)
        {
            int localNc = 0;
            if (IsConficting(g, g[vBest]) ||
                        IsAlreadyAllowablePointColoring
                            (allowablePointColorings, g[vBest].RealNodeNumber, g[vBest].Color))
                foreach (var color in g[vBest].Colors)
                {
                    if (color == g[vBest].Color)
                        continue;
                    if (IsInTabuList(tabuList, g[vBest].RealNodeNumber, color))
                        continue;
                    localNc++;
                }
            foreach (var neighbor in g[vBest].Adjacents)
            {
                if (IsConficting(g, g[neighbor]) ||
                     IsAlreadyAllowablePointColoring
                         (allowablePointColorings, g[neighbor].RealNodeNumber, g[neighbor].Color))
                    foreach (var color in g[neighbor].Colors)
                    {
                        if (color == g[neighbor].Color)
                            continue;
                        if (IsInTabuList(tabuList, g[neighbor].RealNodeNumber, color))
                            continue;
                        localNc++;
                    }
            }
            return localNc;
        }

        public static double RecountMinimizeFunction
            (Graph g, Node node, double fOfC, double alpha, int memColor)
        {
            int curColor = node.Color;
            int conflictedNow = 0;
            int conflictedBefore = 0;
            if (IsConficting(g, node))
                conflictedNow++;
            conflictedNow += GetConflictedNumber(g, node);
            node.Color = memColor;
            if (IsConficting(g, node))
                conflictedBefore++;
            conflictedBefore += GetConflictedNumber(g, node);

            return fOfC + alpha*(conflictedNow - conflictedBefore);
        }

        public static int GetConflictedNumber(Graph g, Node node)
        {
            int conflicted = 0;
            foreach (var neighbor in node.Adjacents)
            {
                if (IsConficting(g, g[neighbor]))
                    conflicted++;
            }
            return conflicted;
        }

        public static bool IsConficting(Graph g, Node node)
            => node.Adjacents.Any(adjacent => g[adjacent].Color == node.Color);
            
        public static double MinimizeFunction(Graph g, double alpha, HashSet<Tuple<int, int>> list)
            => alpha * Func1(g) + Func2(list);


        public static int Func1(Graph g)
        {
            int result = 0;
            foreach (Node node in g.nodes)
            {
                foreach (var neighbor in node.Adjacents)
                {
                    if (node.Color == g[neighbor].Color)
                    {
                        result++;
                        break;
                    }
                }
            }
            return result;
        }

        public static int Func2(HashSet<Tuple<int, int>> list)
            => list.Count;

        public static bool Reduction(this Graph graph)
        {
            bool wasChanges;

            do
            {
                wasChanges = false;
                for (var j = 0; j < graph.Count; j++)
                {
                    Node node = graph.nodes[j];
                    if (node.Colors.Count == 0)
                        return false;
                    if (node.Colors.Count == 1 && node.Adjacents.Any())
                    {
                        var color = node.Colors.ElementAt(0);
                        foreach (var adjacentNodeIndex in node.Adjacents)
                        {
                            if (graph.nodes[adjacentNodeIndex].Colors.Remove(color))
                                wasChanges = true;
                        }
                    }
                    for (var i = node.Adjacents.Count - 1; i >= 0; i--)
                    {
                        int adjacentNodeIndes = node.Adjacents.ElementAt(i);
                        Node adjacentNode = graph.nodes[adjacentNodeIndes];
                        var intersection = node.Colors.Intersect(adjacentNode.Colors);
                        if (!intersection.Any())
                        {
                            node.Adjacents.Remove(adjacentNodeIndes);//////////////////////
                            adjacentNode.Adjacents.Remove(j);
                            wasChanges = true;
                        }
                    }
                }
            } while (wasChanges);
            return true;
        }

        /// <summary>
        /// Возвращает количество цветов, в которые удалось раскрасить граф
        /// Dsatur считает, что вершина может быть окрашена в любой неконфликтующий
        /// цвет
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="coloredCliqueStartedPos">номер вершины в графе, с которой 
        /// начинается уже раскрашенная клика</param>
        /// <returns></returns>
        public static int DSatur(this Graph graph, int coloredCliqueStartedPos)
        {
            SortedDictionary<int, HashSet<int>> nodeNumbersWithSaturation = new SortedDictionary< int, HashSet< int >> ();
            for(int i = 0; i < graph.nodes.Length; i++)
            {
                graph[i].Color = NotColored;
                nodeNumbersWithSaturation.Add(i, new HashSet<int>());
            }
            LinkedList<HashSet<Node>> coloredSets = new LinkedList<HashSet<Node>>();
            while (nodeNumbersWithSaturation.Any())
            {
                int maxSaturation = 0;
                int maxSaturationNodeIndex = 0;
                foreach (var nodeWithSaturation in nodeNumbersWithSaturation)
                {
                    if (nodeWithSaturation.Value.Count > maxSaturation)
                    {
                        maxSaturation = nodeWithSaturation.Value.Count;
                        maxSaturationNodeIndex = nodeWithSaturation.Key;
                    }
                }
                var node = graph[maxSaturationNodeIndex];
                int setIndex;
                for (setIndex = 0; setIndex < coloredSets.Count; setIndex++)
                {
                    bool isIndependentSet = true;
                    HashSet<Node> currentSet = coloredSets.ElementAt(setIndex);
                    foreach (var currentNode in currentSet)
                    {
                        if (currentNode.Adjacents.Contains(maxSaturationNodeIndex))
                        {
                            isIndependentSet = false;
                            break;
                        }
                    }
                    if (isIndependentSet)
                    {
                        currentSet.Add(node);
                        break;
                    }
                }
                if (setIndex == coloredSets.Count)
                {
                    HashSet<Node> newSet = new HashSet<Node> { node };
                    coloredSets.AddLast(newSet);
                }
                foreach (var adjacentNode in node.Adjacents)
                {
                    if (nodeNumbersWithSaturation.ContainsKey(adjacentNode))
                    {
                        nodeNumbersWithSaturation[adjacentNode].Add(setIndex);
                    }
                }
                nodeNumbersWithSaturation.Remove(maxSaturationNodeIndex);
            }
            
            return coloredSets.Count;
        }

        /// <summary>
        /// Очень уж хотелось протестить алгоритм
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="coloredCliqueStartedPos"></param>
        /// <returns></returns>
        public static int DSaturByDenis(this Graph graph, int coloredCliqueStartedPos)
        {
            SortedDictionary<int, HashSet<int>> nodeNumbersWithSaturation = 
                new SortedDictionary<int, HashSet<int>>();
            for (int i = 0; i < coloredCliqueStartedPos; i++)
            {
                graph[i].Color = NotColored;
                nodeNumbersWithSaturation.Add(i, new HashSet<int>());
            }
            LinkedList<HashSet<int>> coloredSets = new LinkedList<HashSet<int>>();
            for (int i = coloredCliqueStartedPos; i < graph.Count; i++)
            {
                coloredSets.AddLast(new HashSet<int>(new[] {i}));
                foreach (int neighbor in graph[i].Adjacents)
                    if (neighbor < coloredCliqueStartedPos)
                        nodeNumbersWithSaturation[neighbor].Add(i);
            }

            while (nodeNumbersWithSaturation.Any())
            {
                int maxSaturation = 0;
                int maxSaturationNodeIndex = 0;
                foreach (var nodeWithSaturation in nodeNumbersWithSaturation)
                {
                    if (nodeWithSaturation.Value.Count > maxSaturation)
                    {
                        maxSaturation = nodeWithSaturation.Value.Count;
                        maxSaturationNodeIndex = nodeWithSaturation.Key;
                    }
                }
                var node = graph[maxSaturationNodeIndex];
                int setIndex;
                for (setIndex = 0; setIndex < coloredSets.Count; setIndex++)
                {
                    bool isIndependentSet = true;
                    HashSet<int> currentSet = coloredSets.ElementAt(setIndex);
                    foreach (var currentNode in currentSet)
                    {
                        if (node.Adjacents.Contains(currentNode))
                        {
                            isIndependentSet = false;
                            break;
                        }
                    }
                    if (isIndependentSet)
                    {
                        currentSet.Add(maxSaturationNodeIndex);
                        node.Color = graph[currentSet.First()].Color;
                        break;
                    }
                }
                if (setIndex == coloredSets.Count)
                {
                    return coloredSets.Count + 1;
                }
                foreach (var adjacentNode in node.Adjacents)
                {
                    if (nodeNumbersWithSaturation.ContainsKey(adjacentNode))
                    {
                        nodeNumbersWithSaturation[adjacentNode].Add(setIndex);
                    }
                }
                nodeNumbersWithSaturation.Remove(maxSaturationNodeIndex);
            }

            return coloredSets.Count;
        }
    }
}
